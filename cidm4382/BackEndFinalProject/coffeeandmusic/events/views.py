from django.shortcuts import render
from django.views.generic import ListView, TemplateView

from .models import Event

# Create your views here.


class EventListView(ListView):
    model = Event
    template_name = 'events.html'

# class AboutView(TemplateView):
#     model = Event
#     template_name = 'events.html'
