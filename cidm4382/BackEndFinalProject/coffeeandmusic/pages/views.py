from django.shortcuts import render
from django.views.generic import TemplateView


class HomePageView(TemplateView):
    template_name = 'home.html'

class TeamPageView(TemplateView):
    template_name = 'ourteam.html'

class MusicianView(TemplateView):
    template_name = 'musicians.html'

class AboutView(TemplateView):
    template_name = 'about.html'
