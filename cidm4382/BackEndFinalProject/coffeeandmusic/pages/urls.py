from django.urls import path
from . import views
from .views import AboutView

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('ourteam/', views.TeamPageView.as_view(), name='ourteam'),
    path('musicians/', views.MusicianView.as_view(), name='musicians'),
    path('about/', views.AboutView.as_view(), name='about'),
]
